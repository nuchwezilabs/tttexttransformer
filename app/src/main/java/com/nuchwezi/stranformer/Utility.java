package com.nuchwezi.stranformer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/*
import rego.PrintLib.printLibException;
import rego.PrintLib.regoPrinter;
*/

public class Utility {

	final static String Tag = "TT";
	private static final long DEFAULT_VIBRATE_TIME = 300;
	public static boolean isPosting = false;

	public static String parseDate(DatePicker datepicker) {
		Date date = new Date(datepicker.getYear() - 1900,
				datepicker.getMonth(), datepicker.getDayOfMonth());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static String parseTime(TimePicker timePicker) {
		String time = null;

		time = String.format("%s:%s", timePicker.getCurrentHour(),
				timePicker.getCurrentMinute());

		return time;
	}

	/*
	 * Will create directory on the External Storage Card with the given dirName
	 * name.
	 *
	 * Throws an exception is dirName is null, and returns the name of the
	 * created directory if successful
	 */
	public static String createSDCardDir(Context context, String dirName, File internalFilesDir) {

		Log.d(Tag, "Creating Dir on sdcard...");

		if (dirName == null) {
			Log.e(Tag, "No Directory Name Specified!");
			return null;
		}

		File exDir =  new File(context.getExternalFilesDir(null) + "/");//Environment.getExternalStorageDirectory();

		if (exDir != null) {

			File folder = new File(exDir, dirName);

			boolean success = false;

			if (!folder.exists()) {
				folder.mkdirs();
				success = folder.exists(); // seems a better approach...
				if(success)
					Log.d(Tag, "Created Dir on sdcard...");

			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		} else {

			File folder = new File(internalFilesDir, dirName);

			boolean success = false;

			if (!folder.exists()) {
				success = folder.mkdirs();
				Log.d(Tag, "Created Dir on sdcard...");
			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		}
	}


	/*
	 * Will create directory on the External Storage Card with the given dirName
	 * name.
	 *
	 * Throws an exception is dirName is null, and returns the name of the
	 * created directory if successful
	 */
	public static String createSDCardDir(String dirName, File internalFilesDir) {

		Log.d(Tag, "Creating Dir on sdcard...");

		if (dirName == null) {
			Log.e(Tag, "No Directory Name Specified!");
			return null;
		}

		File exDir = Environment.getExternalStorageDirectory();

		if (exDir != null) {

			File folder = new File(exDir, dirName);

			boolean success = false;

			if (!folder.exists()) {
				success = folder.mkdirs();
				Log.d(Tag, "Created Dir on sdcard...");
			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		} else {

			File folder = new File(internalFilesDir, dirName);

			boolean success = false;

			if (!folder.exists()) {
				success = folder.mkdirs();
				Log.d(Tag, "Created Dir on sdcard...");
			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		}
	}

	/*
	 * Will create directory on the External Storage Card with the given dirName
	 * name.
	 * 
	 * Throws an exception is dirName is null, and returns the name of the
	 * created directory if successful
	 */
	public static String createSDCardDir(String dirName) {

		Log.d(Tag, "Creating Dir on sdcard...");

		if (dirName == null) {
			Log.e(Tag, "No Directory Name Specified!");
			return null;
		}

		if (Environment.getExternalStorageDirectory() == null) {

			File folder = new File(String.format("%s/%s",
					Environment.getDataDirectory(), dirName));

			boolean success = false;

			if (!folder.exists()) {
				success = folder.mkdir();
				Log.d(Tag, "Created Dir on sdcard...");
			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		} else {

			File folder = new File(String.format("%s/%s",
					Environment.getExternalStorageDirectory(), dirName));

			boolean success = false;

			if (!folder.exists()) {
				success = folder.mkdir();
				Log.d(Tag, "Created Dir on sdcard...");
			} else {
				success = true;
				Log.d(Tag, "Dir exists on sdcard...");
			}

			if (success) {
				return folder.getAbsolutePath();
			} else {
				Log.e(Tag, "Failed to create on sdcard...");
				return null;
			}
		}
	}

	public static ArrayList aToList(String[] parts) {
		ArrayList al = new ArrayList();
		for(int p=0; p<parts.length; p++){
			al.add(parts[p]);
		}
		return  al;
	}

	public static String aJoin(ArrayList lparts, String glue) {
		StringBuilder sl = new StringBuilder();
		for(int s=0;s < lparts.size(); s++)
			sl.append(lparts.get(s)).append(glue == null ? " " : glue);
		return sl.toString().trim();
	}

    public static String anagramize(String input) {
        ArrayList queryl = new ArrayList<String>(Arrays.asList(input.trim().split("")));
        Collections.shuffle(queryl);

        StringBuilder sb = new StringBuilder(queryl.size());
        for(int s = 0; s < queryl.size(); s++){
            sb.append(queryl.get(s));
        }

        return sb.toString();
    }

	public static String humaneDate(Date date) {
		DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm");
		return df.format(date);
	}

	public static String readFileToString(String filePath)  {
		File fl = new File(filePath);
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(fl);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		String ret = null;
		try {
			ret = convertStreamToString(fin);
		} catch (IOException e) {
			return null;
		}
		//Make sure you close all streams.
		try {
			fin.close();
		} catch (IOException e) {
			return null;
		}
		return ret;
	}

	public static String convertStreamToString(InputStream is) throws IOException {
		// http://www.java2s.com/Code/Java/File-Input-Output/ConvertInputStreamtoString.htm
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		Boolean firstLine = true;
		while ((line = reader.readLine()) != null) {
			if(firstLine){
				sb.append(line);
				firstLine = false;
			} else {
				sb.append("\n").append(line);
			}
		}
		reader.close();
		return sb.toString();
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null, otherwise check
		// if we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}


	public static void getHTTP(Context context, String url, final ParametricCallbackJSONArray parametricCallback ) {

		Log.d(Tag, String.format("HTTP GET, FETCH URI: %s", url));

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url(url)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override public void onFailure(Call call, IOException e) {
				e.printStackTrace();
				parametricCallback.call(null);
			}

			@Override public void onResponse(Call call, Response response) throws IOException {
				try (ResponseBody responseBody = response.body()) {
					if (!response.isSuccessful()) {
						parametricCallback.call(null);
					}else {
						JSONArray apiCallStatus = null;
						try {
							apiCallStatus = new JSONArray(response.body().string());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						parametricCallback.call(apiCallStatus);
					}
				}
			}
		});

	}




	/*
	public static void getHTTP(Context context, String url, final ParametricCallbackJSONArray parametricCallback) {

		Log.d(Tag, String.format("HTTP GET, FETCH URI: %s", url));


		Ion.with(context)
				.load(HTTP_METHODS.GET, url)
				.asString()
				.setCallback(new FutureCallback<String>() {
					@Override
					public void onCompleted(Exception e, String result) {

						JSONArray apiCallStatus = null;
						try {
							apiCallStatus = new JSONArray(result);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						parametricCallback.call(apiCallStatus);
					}
				});

	}


	 */
	public static String mirror(String input) {
		if(input == null)
			return null;

		if(input.length() <= 1)
			return input;

		StringBuilder builder = new StringBuilder(input);
		String reversed = builder.reverse().toString();
		return reversed;
	}

    public static String triangular_reduction(String content) {
		ArrayList<String> parts = new ArrayList<>();
		for(int i=0; i < content.length(); i++){
			parts.add(content.substring(i,content.length()));
		}
		return aJoin(parts, "\n");
    }

	public static String rightmost_triangular_reduction(String content) {
		ArrayList<String> parts = new ArrayList<>();
		for(int i=0; i < content.length(); i++){
			parts.add(content.substring(0,content.length()-i));
		}
		return aJoin(parts, "\n");
	}

	public static String humaneDateStripped(Date date,boolean withSeconds) {
		DateFormat df = new SimpleDateFormat(withSeconds ?"yyyyMMMdd__HH_mm_ss" : "yyyyMMMdd__HH_mm");
		return df.format(date);
	}


	public static String readFileToString(FileDescriptor fileDescriptor)  {
		FileInputStream fin = null;
		fin = new FileInputStream(fileDescriptor);
		String ret = null;
		try {
			ret = convertStreamToString(fin);
		} catch (IOException e) {
			return null;
		}
		//Make sure you close all streams.
		try {
			fin.close();
		} catch (IOException e) {
			return null;
		}
		return ret;
	}



	public static class HTTP_METHODS {
		public static final String POST = "POST";
		public static final String GET = "GET";
	}


	public static boolean DeleteFile(String path) {
		File file = new File(path);
		return file.delete();
	}

	/*
	 * Display a toast with the default duration : Toast.LENGTH_SHORT
	 */
	public static void showToast(String message, Context context) {
		showToast(message, context, Toast.LENGTH_SHORT);
	}

	/*
	 * Display a toast with given Duration
	 */
	public static void showToast(String message, Context context, int duration) {
		Toast.makeText(context, message, duration).show();
	}

	public static void showAlert(String title, String message, Context context) {
		showAlert(title, message, R.mipmap.ic_launcher, context);
	}

	public static void showAlert(String title, String message, int iconId,
                                 Context context) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);

			builder.setIcon(iconId);
			builder.setTitle(title);
			builder.setMessage(message);
			// builder.setCancelable(false);

			AlertDialog alert = builder.create();
			alert.show();
		} catch (Exception e) {
			Log.e(Tag, "Alert Error : " + e.getMessage());
		}
	}


	public static void saveBitmapToFile(Bitmap bitmap, String filename,
                                        int qualityPercent) {
		try {
			FileOutputStream out = new FileOutputStream(filename);
			bitmap.compress(Bitmap.CompressFormat.JPEG, qualityPercent, out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String parseDateTime(DatePicker dateP, TimePicker timeP) {
		return String.format("%s %s", parseDate(dateP), parseTime(timeP));
	}

	public static int indexOf(String[] hay, String needle) {
		if (hay.length == 0)
			return -1;

		for (int i = 0; i < hay.length; i++)
			if (hay[i].equals(needle))
				return i;

		return -1;
	}

	public static class KEYS {
		public static final String APP_PREFERENCES = "KEYS";
		public static final String CACHE_SCRIPTS = "SCRIPTS";
	}



	public static void setSetting(String KEY, String VALUE, Context context) {
		SharedPreferences prefs = context.getSharedPreferences(
                KEYS.APP_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(KEY, VALUE);
		editor.commit();
	}

	public static String getSetting(String KEY, String DEFAULT, Context context) {
		SharedPreferences prefs = context.getSharedPreferences(
                KEYS.APP_PREFERENCES, Context.MODE_PRIVATE);
		return prefs.getString(KEY, DEFAULT);
	}

	public static boolean hasSetting(String KEY, Context context) {
		SharedPreferences prefs = context.getSharedPreferences(
                KEYS.APP_PREFERENCES, Context.MODE_PRIVATE);
		return prefs.contains(KEY);
	}


	public static String getDateTimeNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		GregorianCalendar now = new GregorianCalendar();
		return sdf.format(now.getTime());
	}
	
	public static String getTimeNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		GregorianCalendar now = new GregorianCalendar();
		return sdf.format(now.getTime());
	}
	
	public static String getDateNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar now = new GregorianCalendar();
		return sdf.format(now.getTime());
	}


	public static JSONArray removeFromJSONArray(JSONArray jArray, String xml) {

		JSONArray newArray = new JSONArray();
		for (int r = 0; r < jArray.length(); r++) {
			String s = null;
			try {
				s = jArray.getString(r);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (s != null)
				if (!s.equalsIgnoreCase(xml))
					try {
						newArray.put(jArray.get(r));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		}

		return newArray;
	}


	public static int getVersionNumber(Context context) {
		PackageInfo pinfo = null;
		try {
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pinfo != null ? pinfo.versionCode : 1;
	}

	public static String getVersionName(Context context) {
		PackageInfo pinfo = null;
		try {
			pinfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pinfo != null ? pinfo.versionName : "DEFAULT";
	}

}
