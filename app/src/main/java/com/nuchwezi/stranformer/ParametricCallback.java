package com.nuchwezi.stranformer;

public interface ParametricCallback {
    void call(String status);
}
