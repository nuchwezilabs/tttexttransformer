package com.nuchwezi.stranformer;

import org.json.JSONArray;

public interface ParametricCallbackJSONArray {
    void call(JSONArray status);
}
