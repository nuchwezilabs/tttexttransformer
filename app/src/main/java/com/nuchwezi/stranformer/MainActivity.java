package com.nuchwezi.stranformer;

import static android.icu.lang.UCharacter.LineBreak.GLUE;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.ipaulpro.afilechooser.utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private static final String DATACACHE_BASEDIR = "TT_TEA_SCRIPTS";
    private static final String TAG = "TT";
    private static final String TEA_GLUE = " ";
    private String activeTransformerScript;
    DBAdapter dbAdapter;
    ArrayList<String> scriptNames = new ArrayList<>();
    ArrayList<String> scriptCode = new ArrayList<>();
    private int currentSelectedScriptIndex = -1;

    private static class INTENT_MODE {

        public static final int CHOOSE_TTTEAS_FILE_REQUESTCODE = 3;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbAdapter = new DBAdapter(this);
        dbAdapter.open();

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }

        bootstrapHandlers();

        showAppVersion();

        Log.e(TAG, "TTTT ready!");
    }

    private void showAppVersion() {
        setTitle(String.format("%s [%s:%s]", getString(R.string.app_name), Utility.getVersionNumber(this), Utility.getVersionName(this)));
    }

    private void bootstrapHandlers() {

        loadCachedScripts();

        Button btnRun = findViewById(R.id.btnScriptRun);
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parseAndRunScript();
            }
        });

        Button btnSave = findViewById(R.id.btnScriptSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveCurrentScript();
            }
        });

        Button btnUse = findViewById(R.id.btnScriptUse);
        btnUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadSelectedScript();
            }
        });

        Button btnDel = findViewById(R.id.btnScriptDel);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteSelectedScript();
            }
        });

        Button btnShare = findViewById(R.id.btnOutputShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shareOutput();
            }
        });


        Button btnBackPropagate = findViewById(R.id.btnOutputBackPropagate);
        btnBackPropagate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backPropagateTransformOutput();
            }
        });


    }

    private void backPropagateTransformOutput() {
        String transformOutput = ((TextView)findViewById(R.id.txtOutput)).getText().toString();
        if(transformOutput.length() == 0)
            return;

        EditText txtView = findViewById(R.id.eTxtInput);
        txtView.setText(transformOutput);
    }

    private void shareOutput() {
        String transformOutput = ((TextView)findViewById(R.id.txtOutput)).getText().toString();
        if(transformOutput.length() == 0)
            return;

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, transformOutput);
        startActivity(Intent.createChooser(sharingIntent, "Share Transformed Text..."));
    }

    private void deleteSelectedScript() {
        if(currentSelectedScriptIndex >= 0){
            String sCachedScript = dbAdapter.fetchDictionaryEntry(Utility.KEYS.CACHE_SCRIPTS);
            try {
                JSONArray jCachedScript = new JSONArray(sCachedScript);
                jCachedScript.remove(currentSelectedScriptIndex);
                dbAdapter.updateDictionaryEntry(new DBAdapter.DictionaryKeyValue(Utility.KEYS.CACHE_SCRIPTS, jCachedScript.toString()));
                loadCachedScripts();
                currentSelectedScriptIndex -= 1;
                loadSelectedScript();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadSelectedScript() {
        if(currentSelectedScriptIndex >= 0){
            ((EditText)findViewById(R.id.eTxtScript)).setText(scriptCode.get(currentSelectedScriptIndex));
            ((EditText)findViewById(R.id.eTxtScriptName)).setText(scriptNames.get(currentSelectedScriptIndex));
        }
    }

    private void loadCachedScripts() {

         scriptNames = new ArrayList<>();
         scriptCode = new ArrayList<>();

        String sCachedScript = dbAdapter.fetchDictionaryEntry(Utility.KEYS.CACHE_SCRIPTS);
        if(sCachedScript == null)
            return;

        try {
            JSONArray jCachedScript = new JSONArray(sCachedScript);
            for(int s=0; s < jCachedScript.length(); s++){
                String script = jCachedScript.getString(s);
                String[] parts = script.split("\\|",2);

                scriptNames.add(parts[0]);
                scriptCode.add(parts[1]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        bootstrapScriptSpinner(scriptNames);
    }

    private void bootstrapScriptSpinner(ArrayList<String> names) {
        Spinner spinnerScripts = findViewById(R.id.spinnerScripts);
        ArrayAdapter<String> nAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, names);
        spinnerScripts.setAdapter(nAdapter);

        spinnerScripts.setOnItemSelectedListener (new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                currentSelectedScriptIndex = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void saveCurrentScript() {

        String transformerScript = ((EditText) findViewById(R.id.eTxtScript)).getText().toString();
        if (transformerScript.length() == 0) {
            Utility.showToast("There's no Transform Script provided yet!", this);
            return;
        }

        String scriptName = ((EditText) findViewById(R.id.eTxtScriptName)).getText().toString();
        if (scriptName.length() == 0) {
            Utility.showToast("Please specify a Script Name!", this);
            return;
        }

        String cacheScript = String.format("%s|%s", scriptName, transformerScript);
        //dbAdapter.insertList(Utility.KEYS.CACHE_SCRIPTS, cacheScript);
        dbAdapter.insertSET(Utility.KEYS.CACHE_SCRIPTS, cacheScript);

        loadCachedScripts();
    }

    private void parseAndRunScript() {
        final String inputContent = ((EditText)findViewById(R.id.eTxtInput)).getText().toString();

        if(activeTransformerScript == null) {
            String transformerScript = ((EditText) findViewById(R.id.eTxtScript)).getText().toString();
            if (transformerScript.length() == 0) {
                renderTransformOutput(new TransformerOutput(inputContent, inputContent)); // in the absence of any transform script, the input is the output
            }
            activeTransformerScript = transformerScript;
        }

        TransformerOutput tOutput = applyTransform(activeTransformerScript, inputContent);
        renderTransformOutput(tOutput);
        activeTransformerScript = null;
    }

    public static class TransformerOutput {
        public String TransformOutput;
        public String ActiveTransformInput;

        public TransformerOutput(){
        }
        public TransformerOutput(String activeInput, String activeOutput){
            this.ActiveTransformInput = activeInput;
            this.TransformOutput = activeOutput;
        }
    }

    private void renderTransformOutput(TransformerOutput tOutput) {
        ((TextView)findViewById(R.id.txtOutput)).setText(tOutput.TransformOutput);
        computeAndShowMetrics(tOutput);
    }

    private void computeAndShowMetrics(TransformerOutput tOutput) {
        String inputStr =  tOutput.ActiveTransformInput; //((TextView)findViewById(R.id.eTxtInput)).getText().toString();
        String outputStr =  tOutput.TransformOutput; //((TextView)findViewById(R.id.txtOutput)).getText().toString();

        TextView txtMetrics =  ((TextView)findViewById(R.id.txtMetrics));

        int ICC = inputStr.length(); // input character count
        int OCC = outputStr.length(); //output character count
        double TransformRatio = Math.abs(OCC - ICC) / (ICC * 1.0);

        String metrics = String.format("Analyzing current TEA Program:\n\n-> Input Character Count: %s\n-> Output Character Count: %s\n>> [Transform Ratio] |OCC-ICC|:ICC = %.2f (%.2f%%)",
                ICC, OCC, TransformRatio, TransformRatio * 100);

        txtMetrics.setText(metrics);
    }

    private TransformerOutput applyTransform(String transformerScript, String inputString) {

        TransformerOutput tOutput = new TransformerOutput();

        String[] instructions = transformerScript.split("\n");
        String currentTransformOutput = inputString;

        // note: this could be overridden upon occurence of i: command in the script
        String activeInputString = inputString;

        for(int s=0; s<instructions.length; s++){
            String command = instructions[s];
            if(command.toUpperCase().startsWith("R:")){ // replace: r:pattern:replace
                String[] tokens = command.split(":",3);
                if(currentTransformOutput == null){
                    currentTransformOutput = activeInputString.replaceAll(tokens[1],tokens[2]).replaceAll("[\\\\]n","\n");
                }else {
                    currentTransformOutput = currentTransformOutput.replaceAll(tokens[1],tokens[2]).replaceAll("[\\\\]n","\n");
                }
            } else  if(command.toUpperCase().startsWith("D:")){ // delete : d:pattern
                String[] tokens = command.split(":",2);
                if(currentTransformOutput == null){
                    currentTransformOutput = activeInputString.replaceAll(tokens[1],"");
                }else {
                    currentTransformOutput = currentTransformOutput.replaceAll(tokens[1],"");
                }
            }
            else  if(command.toUpperCase().startsWith("K:")){ // keep : k:pattern
                String[] tokens = command.split(":",2);
                if(currentTransformOutput == null){
                    String[] inputLines = activeInputString.split("\\r?\\n");
                    ArrayList<String> keptLines = new ArrayList<>();
                    for(int i = 0; i < inputLines.length; i++){
                        if(inputLines[i].matches(tokens[1]))
                            keptLines.add(inputLines[i]);
                    }
                    currentTransformOutput = String.join(System.lineSeparator(), keptLines);
                }else {
                    String[] inputLines = currentTransformOutput.split("\\r?\\n");
                    ArrayList<String> keptLines = new ArrayList<>();
                    for(int i = 0; i < inputLines.length; i++){
                        if(inputLines[i].matches(tokens[1]))
                            keptLines.add(inputLines[i]);
                    }
                    currentTransformOutput = String.join(System.lineSeparator(), keptLines);
                }
            }
            else  if(command.toUpperCase().startsWith("S:")){ // shuffle: s:
                if(currentTransformOutput == null){
                    String[] parts = activeInputString.split("\\s+");
                    ArrayList lparts = Utility.aToList(parts);
                    Collections.shuffle(lparts);
                    currentTransformOutput = Utility.aJoin(lparts,TEA_GLUE);
                }else {
                    String[] parts = currentTransformOutput.split("\\s+");
                    ArrayList lparts = Utility.aToList(parts);
                    Collections.shuffle(lparts);
                    currentTransformOutput = Utility.aJoin(lparts,TEA_GLUE);
                }
            }else  if(command.toUpperCase().startsWith("AA:")){ // anagramize input: aa:
                if(currentTransformOutput == null){
                    currentTransformOutput = Utility.anagramize(activeInputString);
                }else {
                    currentTransformOutput = Utility.anagramize(currentTransformOutput);
                }
            }
            else  if(command.toUpperCase().startsWith("A:")){ // anagramize words in place: a:
                if(currentTransformOutput == null){
                    String[] parts = activeInputString.split("\\s+");
                    ArrayList<String> lparts = Utility.aToList(parts);
                    for(int i=0;i<lparts.size();i++)
                        lparts.set(i, Utility.anagramize((lparts.get(i))));

                    currentTransformOutput = Utility.aJoin(lparts,TEA_GLUE);
                }else {
                    String[] parts = currentTransformOutput.split("\\s+");
                    ArrayList<String> lparts = Utility.aToList(parts);
                    for(int i=0;i<lparts.size();i++)
                        lparts.set(i, Utility.anagramize((lparts.get(i))));

                    currentTransformOutput = Utility.aJoin(lparts,TEA_GLUE);
                }
            }
            else  if(command.toUpperCase().startsWith("T:")){ // triangular reduction: t:
                if(currentTransformOutput == null){
                    currentTransformOutput = Utility.triangular_reduction(activeInputString);
                    //Log.e(Tag, currentTransformOutput);
                }else {
                    currentTransformOutput = Utility.triangular_reduction(currentTransformOutput);
                    //Log.e(Tag, currentTransformOutput);
                }
            }
            else  if(command.toUpperCase().startsWith("RT:")){ // rightmost triangular reduction: rt:
                if(currentTransformOutput == null){
                    currentTransformOutput = Utility.rightmost_triangular_reduction(activeInputString);
                    //Log.e(Tag, currentTransformOutput);
                }else {
                    currentTransformOutput = Utility.rightmost_triangular_reduction(currentTransformOutput);
                    //Log.e(Tag, currentTransformOutput);
                }
            }
            else  if(command.toUpperCase().startsWith("M:")){ // laterally invert words (mirror): m:
                if(currentTransformOutput == null){
                    String[] parts = activeInputString.split("\\s+");
                    ArrayList<String> lparts = Utility.aToList(parts);
                    for(int i=0;i<lparts.size();i++)
                        lparts.set(i, Utility.mirror((lparts.get(i))));

                    currentTransformOutput = Utility.aJoin(lparts," ");
                }else {
                    String[] parts = currentTransformOutput.split("\\s+");
                    ArrayList<String> lparts = Utility.aToList(parts);
                    for(int i=0;i<lparts.size();i++)
                        lparts.set(i, Utility.mirror((lparts.get(i))));

                    currentTransformOutput = Utility.aJoin(lparts," ");
                }
            }
            else  if(command.toUpperCase().startsWith("MM:")){ // laterally invert everything (mirror): mm:
                if(currentTransformOutput == null){
                    currentTransformOutput = Utility.mirror(activeInputString);
                }else {
                    currentTransformOutput = Utility.mirror(currentTransformOutput);
                }
            }
            else  if(command.toUpperCase().startsWith("I:")){ // i:STRING --> inject explicit input STRING as active input
                /*
                Given example script:
                i:start
                r:st:p

                And no input, i: shall take whatever is written after the ":" on the line with the command, and set it
                as the active input to any subsequent commands. Thus, the above example script would always output "part" where
                no input was given.

                When multiple instances of the i: command exist in a script, only the first occurrence has effect, and all the
                others are ignored, unless the current output (which is the active input to the next command) at the time the i: is executed
                is blank or null.

                So,

                i:start
                i:west
                r:st:p

                Shall always output "part" where no explicit input was given, but

                i:start
                r:st:p
                d:part
                i:west
                r:w:b

                shall always output "best" where no explicit input was given.
                 */

                if((currentTransformOutput == null) || (currentTransformOutput.length() == 0)){
                    // set both activeInput and currentTransformerOutput to given input
                    // makes sense either if the i: command was the first in the script, or
                    // some earlier commands have since set currentTransformerOutput to blank/null -- which would have left subsequent
                    // commands with nothing to process...
                    String[] tokens = command.split(":",2);
                    activeInputString = tokens[1];
                    currentTransformOutput = activeInputString;
                }
            }

        }

        tOutput.TransformOutput = currentTransformOutput;
        tOutput.ActiveTransformInput = activeInputString;

        return tOutput;
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            EditText txtView = (EditText) findViewById(R.id.eTxtInput);
            txtView.setText(sharedText);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_about: {
                showAbout();
                return true;
            }
            case R.id.action_export: {
                exportTT_TEA_Scripts_ToFile();
                return true;
            }
            case R.id.action_import: {
                importTT_TEA_Scripts_FromFile();
                return true;
            }
            case R.id.action_import_default: {
                importRecordsFromOnlineConfig();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void importRecordsFromOnlineConfig() {

        if (!Utility.isNetworkAvailable(this)) {
            Utility.showToast("Sorry, but you need an active connection to import the online TEAS!", MainActivity.this);
            return;
        }

        Utility.showToast("Wait as TEAS are fetched online...", MainActivity.this, Toast.LENGTH_LONG);
        Utility.getHTTP(this, getString(R.string.tttt_default_example_teas_json),
                new ParametricCallbackJSONArray() {
                    @Override
                    public void call(JSONArray data) {
                        if (data == null) {
                            Utility.showToast("Error importing TEAS. Check connectivity and try again", MainActivity.this);
                        } else {

                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {

                                    processImportedTEAS(data);
                                    Utility.showToast("TEAS successfully loaded. You may proceed to use TTTT.", MainActivity.this, Toast.LENGTH_LONG);
                                }
                            });


                        }
                    }
                });

    }

    private boolean hasPermissionReadStorage() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean getOrRequestReadStoragePermission() {
        if(hasPermissionReadStorage()){
            return true;
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
        }

        return false;
    }

    private void importTT_TEA_Scripts_FromFile() {
/*
        if(!getOrRequestReadStoragePermission()){
            Utility.showToast("Please allow the app to read from your storage first.", this);
            return;
        }

        String personaMimeType = getString(R.string.mimeType_TEAS_datafile);

        // Create the ACTION_GET_CONTENT Intent
        Intent getContentIntent = FileUtils.createGetContentIntent();

        getContentIntent.setType(personaMimeType);
        getContentIntent.addCategory(Intent.CATEGORY_OPENABLE);

        Intent chooserIntent = Intent.createChooser(getContentIntent, getString(R.string.label_TEAS_from_file));


        try {
            startActivityForResult(chooserIntent, INTENT_MODE.CHOOSE_TTTEAS_FILE_REQUESTCODE);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), R.string.error_no_file_manager_found, Toast.LENGTH_SHORT).show();
        }*/

        if(!getOrRequestReadStoragePermission()){
            Utility.showToast("Please allow the app to read from your storage first.", this);
            return;
        }

        String metaEggMimeType = getString(R.string.mimeType_TEAS_datafile);

        // Create the ACTION_GET_CONTENT Intent
        // Intent getContentIntent = FileUtils.createGetContentIntent();
        // using recommended approach: https://developer.android.com/training/data-storage/shared/documents-files#java
        Intent getContentIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        getContentIntent.addCategory(Intent.CATEGORY_OPENABLE);
        getContentIntent.setType(metaEggMimeType);


        Intent chooserIntent = Intent.createChooser(getContentIntent, getString(R.string.label_tttt_from_file));


        try {
            startActivityForResult(chooserIntent, INTENT_MODE.CHOOSE_TTTEAS_FILE_REQUESTCODE);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), R.string.error_no_file_manager_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode){
            case INTENT_MODE.CHOOSE_TTTEAS_FILE_REQUESTCODE: {
                if(intent == null) {
                    Utility.showToast("Failed to perform action", this);
                    break;
                }

                final Uri uri = intent.getData();
/*
                // Get the File path from the Uri
                String selectedPath = FileUtils.getPath(this, uri);

                loadImportedTTTEASFromPath(selectedPath);*/

                //String selectedPath = intent.getDataString();

                // Get the File path from the Uri
                // Get the File path from the Uri
                //String selectedPath = FileUtils.getPath(this, uri);

                ParcelFileDescriptor parcelFileDescriptor =
                        null;
                try {
                    parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                    loadImportedTTTEASFromPath(fileDescriptor);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


                break;
            }
        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void loadImportedTTTEASFromPath(FileDescriptor fileDescriptor) {
        String sCacheRecords_Imported  = null;
        try {
            sCacheRecords_Imported = Utility.readFileToString(fileDescriptor);

            JSONArray importedTEAS = new JSONArray(sCacheRecords_Imported);

            processImportedTEAS(importedTEAS);

        } catch (Exception e) {
            e.printStackTrace();
            Utility.showAlert("Import File Error","Sorry, but loading the TTTT TEAS from file has failed! Ensure the file can be read, and is legitimate!",
                    R.mipmap.ic_launcher,this);
            return;
        }
    }

    private void processImportedTEAS(JSONArray importedTEAS) {


        if(!dbAdapter.existsDictionaryKey(Utility.KEYS.CACHE_SCRIPTS)){
            dbAdapter.createDictionaryEntry(new DBAdapter.DictionaryKeyValue(Utility.KEYS.CACHE_SCRIPTS, importedTEAS.toString()));
        }

        // update/merge...
        for(int i =0; i < importedTEAS.length(); i++){
            try {
                dbAdapter.insertSET(Utility.KEYS.CACHE_SCRIPTS, importedTEAS.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        loadCachedScripts();
    }



    private boolean getOrRequestWriteStoragePermission() {
        if(hasPermissionWriteStorage()){
            return true;
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }

        return false;
    }

    private boolean hasPermissionWriteStorage() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void exportTT_TEA_Scripts_ToFile() {
        if(!getOrRequestWriteStoragePermission()){
            Utility.showToast("Please allow TT to be able write to your storage first.", this);
            return;
        }


        if(dbAdapter.existsDictionaryKey(Utility.KEYS.CACHE_SCRIPTS)) {


            String sCacheRecords = dbAdapter.fetchDictionaryEntry(Utility.KEYS.CACHE_SCRIPTS);

            String dataPath = null;

            try {
                dataPath = Utility.createSDCardDir(this, DATACACHE_BASEDIR, getFilesDir());
            } catch (Exception e) {
                Log.e(TAG, "DATA Path Error : " + e.getMessage());
                Utility.showToast(e.getMessage(), getApplicationContext(),
                        Toast.LENGTH_LONG);
            }

            if(dataPath != null) {

                String SESSION_GUUID = java.util.UUID.randomUUID().toString().substring(0,8);
                String dataCacheFile = String.format("%s/%s-%s.%s", dataPath, Utility.humaneDateStripped(new Date(), true), SESSION_GUUID,
                        "txt");

                Writer output = null;
                File file = new File(dataCacheFile);
                try {
                    output = new BufferedWriter(new FileWriter(file));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    output.write(sCacheRecords);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Utility.showToast(String.format("%s Data Cached at : %s", getString(R.string.app_name), dataCacheFile), this);

                previewOrCopyFileExport(this, file);
            }
        }
    }



    private void previewOrCopyFileExport(Context context, File file) {

        Intent intent = new Intent(Intent.ACTION_VIEW);


        Uri mURI = FileProvider.getUriForFile(
                context,
                context.getApplicationContext()
                        .getPackageName() + ".provider", file);
        intent.setDataAndType(mURI, "text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            context.startActivity(intent);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showAbout() {
        Utility.showAlert(
                this.getString(R.string.app_name),
                String.format("Version %s (Build %s)\n\n%s",
                        Utility.getVersionName(this),
                        Utility.getVersionNumber(this),
                        this.getString(R.string.powered_by)),
                R.mipmap.ic_launcher, this);
    }


}
