# TTTT: TEA Text Transforming Terminal
### This is the original TTTT mobile application for Android OS

The TTTT app (currently unavailable on the Google Play Store - see: https://bit.ly/grabteas) was the first attempt to implement the TEA computer
computer programming language. The language was then implemented using the Android OS native language Java, and the environment was designed
to allow for interactive explorations of TEA programming from within TTTT, with a facility for composing input, editing new or existing/cached
TEA programs, and also being able to run the programs and share or feedback the outputs. Also, the mobile TTTT does offer a facility to fetch and 
load a set of standard TEA programs created and curated by the TEA inventor, to allow for demonstrating the usefulness of TEA, 
examples of syntax and more.

# To INSTALL and TEST Mobile TTTT?

Since the app isn't currently accessible on the Store, one might test it by cloning this repository and building their own APK for those who can. 
Otherwise, one could search in this repository for a pre-built APK, or one might search the web Android APP repositories - such as APKPure.com, 
for older and recent versions of the app.

# Is Mobile TTTT still the reference for TEA programming?

At the moment (25th Aug, 2024), much of the progress on the TEA programming language, as well as the TTTT environment is happening on the
more generic, system-agnostic version of TTTT - see https://github.com/mcnemesis/cli_tttt/

The Command Line CLI version of TTTT linked to above, is not only ahead in terms of its coverage of the most recent TEA programming language 
standard, but is likely to stay ahead of the mobile app for the fore-seable future. This is so, focus can first be placed on refining and 
perfecting that reference implementation, so it becomes the standard all other TEA implementations - such as planned Web-based TTTT environment, 
base or reference.

